This repository is to demonstrate a basic collection of Terraform manifest. This collection can be used to architect and basic VPC with essential resources using IaaC to launch a simple index.html.

Using this collection:

We establish a VPC with 3-tiers for private, public, and database infrastructure. Each tier is built upon 2 different availability zones (us-east-1 & us-east-2) with a total of 6 subnets. A local values manifest is included to define ownership (i.e.- team) of resources and tagging of environments (i.e.- dev, qa.) NAT gateway is configured for database connectivity and an elastic IP is configured for bashtion host connectivity. Null resource manifest serves as the provisioner for copying the PLAYONDEMOAPP.pem key for ssh connection into instances.

A security group is created to enable port 22 and port 80, for SSH and HTTP access. A target group is created for HTTP traffic for our load balancer on port 80. A path-based application load balancer is created to access our HTTP target groups with each of the AZ’s defined.

2 EC2 Terraform manifest were created to provision (3x t2.micro instances. These instances serve as the bashtion host (1x) and for the )private application layer (2x) with user data that creates the index.html file. 

(Not included is Jenkinsfile for CI/CD pipeline and RDS volumes for persistent storage.)

